using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyAnimattion : MonoBehaviour
{
    private EnemyAttack enemyAttack;

    private void Awake()
    {
        enemyAttack = GetComponentInParent<EnemyAttack>();
    }

    public void AttackMoment()
    {
        enemyAttack.AttackTarget();
    }
}   
