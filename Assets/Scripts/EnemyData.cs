using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
[RequireComponent(typeof(NavMeshAgent))]
public class EnemyData : MonoBehaviour
{
    [SerializeField] private string playerTag = "Player";
    public Transform target;
    public Health targetHealth;
    public Animator animator;
    public NavMeshAgent agent;
    public float distanceToTarget;

    private void Awake()
    {
        targetHealth = target.GetComponent<Health>();
        agent = GetComponent<NavMeshAgent>();
        animator = GetComponentInChildren<Animator>();
        if (target == null) target = GameObject.FindWithTag("Player").transform;
    }

    private void Update()
    {
        distanceToTarget = Vector3.Distance(target.position, transform.position);
    }
}
