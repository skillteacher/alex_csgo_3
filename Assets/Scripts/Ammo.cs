using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class Ammo : MonoBehaviour
{
    [SerializeField] private int ammoAmount = 20;
    [SerializeField] private TextMeshProUGUI ammoText;
}
