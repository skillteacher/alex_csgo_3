using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(EnemyData))]
public class EnemyAttack : MonoBehaviour
{
    [SerializeField] private float damage = 10f;
    [SerializeField] private float range = 2f;
    private EnemyData enemyData;

    private void Awake()
    {
        enemyData = GetComponent<EnemyData>();
    }


    private void Update()
    {
        enemyData.animator.SetBool("attack", enemyData.distanceToTarget > range ? false : true);
    }

    public void AttackTarget()
    {
        enemyData.targetHealth.TakeDamage(damage);
    }
}
