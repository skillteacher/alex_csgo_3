using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Portal : MonoBehaviour
{
    [SerializeField] private string nextLevelName;
    [SerializeField] private string playerTag = "Player";

    private void OnTriggerEnter(Collider other) 
    {
        if(other.gameObject.CompareTag(playerTag)) SceneManager.LoadScene(nextLevelName);
    }
}
